# Inputs for this scripts are $TAILSCALE_VERSION , $TAILSCALE_AUTHKEY , $TAILSCALE_HOSTNAME
# Inspired from https://github.com/tailscale/github-action
apk --no-cache add curl
MINOR=$(echo "$TAILSCALE_VERSION" | awk -F '.' {'print $2'})
if [ $((MINOR % 2)) -eq 0 ]; then
    URL="https://pkgs.tailscale.com/stable/tailscale_${TAILSCALE_VERSION}_amd64.tgz"
else
    URL="https://pkgs.tailscale.com/unstable/tailscale_${TAILSCALE_VERSION}_amd64.tgz"
fi
curl "$URL" -o tailscale.tgz
tar -C /tmp -xzf tailscale.tgz
rm tailscale.tgz
TSPATH=/tmp/tailscale_${TAILSCALE_VERSION}_amd64
mv "${TSPATH}/tailscale" "${TSPATH}/tailscaled" /usr/bin

tailscaled --tun=userspace-networking 2>~/tailscaled.log & # This is required inside docker containers if you are not mounting /dev/net/tun
tailscale up --authkey ${TAILSCALE_AUTHKEY} --hostname=${TAILSCALE_HOSTNAME} --accept-routes
