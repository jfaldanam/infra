terraform {
  backend "http" {
    # Add Gitlab username and password (API key with api access) on init
    address        = "https://gitlab.com/api/v4/projects/42947656/terraform/state/jfaldanam-infra"
    lock_address   = "https://gitlab.com/api/v4/projects/42947656/terraform/state/jfaldanam-infra/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/42947656/terraform/state/jfaldanam-infra/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

module "oracle-1" {
  source  = "./modules/oracle-1"
  ip      = var.oracle_1_ip
  user    = var.user
  ssh_key = var.ssh_key
  port    = var.port
}

module "oracle-2" {
  source  = "./modules/oracle-2"
  ip      = var.oracle_2_ip
  user    = var.user
  ssh_key = var.ssh_key
  port    = var.port
}

# Raspberry Pi SD card is dead, moving components to other VMs
#module "raspberry-pi" {
#  source                = "./modules/raspberry-pi"
#  ip                    = var.raspberry_pi_ip
#  user                  = var.user
#  ssh_key               = var.ssh_key
#  port                  = var.port
#  transmission_user     = var.pi_transmission_user
#  transmission_password = var.pi_transmission_password
#}