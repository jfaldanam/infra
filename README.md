# jfaldanam's cloud
Current status: [![pipeline status](https://gitlab.com/jfaldanam/infra/badges/master/pipeline.svg)](https://gitlab.com/jfaldanam/infra/-/commits/master)

This repository contains the Infrastructure as Code (IaC) to describe my "homelab", running on a Raspberry Pi 3B and free-tier cloud services.

# Technologies
Currently, it uses:
* [Terraform](https://www.terraform.io/): As a tool to define my Infrastructure as Code
* [Gitlab](https://gitlab.com): For storing my remote .tfstate and execute the terraform commands using CI/CD on a self-hosted [gitlab-runner](https://docs.gitlab.com/runner/).
* [Tailscale](https://tailscale.com/): To create a secure peer2peer VPN between my computers and give the gitlab runners ephemeral access to my network to apply the changes triggered by the CI/CD pipeline.
* [Docker](https://www.docker.com/): To manage the services running on my nodes. Kubernetes is overkill for my usages.

![architecture diagram](assets/architecture.svg)

\*Technically, the CI/CD runner is also on the same machines, but it is not required.

# "Hardware"
To set up a cross monitoring of services in case things go down, currently I have deployments in:
* A Raspberry Pi 3B, running headless @ my home. 4 CPUs and 1 GB memory.
* Two always-free VMs at [Oracle Cloud Infrastructure (OCI)](https://www.oracle.com/cloud/free/) (it now requires a credit card to sign up). 2 vCPUs and 1GB memory each.
