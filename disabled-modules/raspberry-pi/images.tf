resource "docker_image" "portainer-agent" {
  name = "portainer/agent:2.9.3"
}

resource "docker_image" "transmission" {
  name = "linuxserver/transmission:version-3.00-r8"
}