resource "docker_container" "portainer-agent" {
  name  = "portainer-agent"
  image = docker_image.portainer-agent.image_id

  restart = "always"

  volumes {
    container_path = "/var/run/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    container_path = "/var/lib/docker/volumes"
    host_path = "/var/lib/docker/volumes"
  }

  ports { # Portainer Agent port
    internal = 9001
    external = 9001
    protocol = "tcp"
  }
}

resource "docker_container" "transmission" {
  name  = "transmission"
  image = docker_image.transmission.image_id

  restart = "always"

  env = ["TZ=Europe/Madrid", "TRANSMISSION_WEB_HOME=/flood-for-transmission/", "USER=${var.transmission_user}", "PASS=${var.transmission_password}"]

  volumes {
    container_path = "/config"
    host_path = "/docker_volumes/config"
  }

  volumes {
    container_path = "/downloads"
    host_path = "/docker_volumes/data"
  }

  ports { # Web UI
    internal = 9091
    external = 9091
    protocol = "tcp"
  }

  ports { # Torrent
    internal = 51413
    external = 51413
    protocol = "tcp"
  }

    ports { # Torrent UDP
    internal = 51413
    external = 51413
    protocol = "udp"
  }
}