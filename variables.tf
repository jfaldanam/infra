variable "oracle_1_ip" {
  type        = string
  description = "IP address of the host 'oracle-1'."
}

variable "oracle_2_ip" {
  type        = string
  description = "IP address of the host 'oracle-2'."
}

variable "raspberry_pi_ip" {
  type        = string
  description = "IP address of the host 'raspberry-pi'."
}

variable "port" {
  type        = number
  description = "Port of the ssh daemon on all hosts."
  default     = 22
}

variable "user" {
  type        = string
  description = "User to connect to any of the hosts through ssh."
}

variable "ssh_key" {
  type        = string
  description = "Path of the ssh private key to connect any of the host."
  sensitive   = true
}

variable "pi_transmission_user" {
  type        = string
  description = "Username for the transmission service"
}

variable "pi_transmission_password" {
  type        = string
  description = "Username for the transmission service"
  sensitive   = true
}