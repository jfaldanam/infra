resource "docker_image" "portainer-agent" {
  name = "portainer/agent:2.9.3"
}

resource "docker_image" "pi-hole" {
  name = "pihole/pihole:latest"
}

resource "docker_image" "syncthing" {
  name = "syncthing/syncthing:latest"
}

resource "docker_image" "minio" {
  name = "minio/minio:RELEASE.2023-07-21T21-12-44Z"
}