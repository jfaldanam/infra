resource "docker_container" "portainer-agent" {
  name  = "portainer-agent"
  image = docker_image.portainer-agent.image_id

  restart = "always"

  volumes {
    container_path = "/var/run/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    container_path = "/var/lib/docker/volumes"
    host_path = "/var/lib/docker/volumes"
  }

  ports { # Portainer Agent port
    internal = 9001
    external = 9001
    protocol = "tcp"
  }
}

resource "docker_container" "pi-hole" {
  name  = "pi-hole"
  image = docker_image.pi-hole.image_id

  restart = "always"

  env = ["TZ=Europe/Madrid"]

  volumes {
    container_path = "/etc/pihole"
    host_path = "/docker_volumes/pihole/pihole"
  }

  volumes {
    container_path = "/etc/dnsmasq.d"
    host_path = "/docker_volumes/pihole/dnsmasq.d"
  }

  ports { # DNS on UDP protocol
    internal = 53
    external = 53
    protocol = "tcp"
  }

  ports { # DNS on UDP protocol
    internal = 53
    external = 53
    protocol = "udp"
  }

  ports { # Web UI
    internal = 80
    external = 80
    protocol = "tcp"
  }
}

resource "docker_container" "syncthing" {
  name  = "syncthing"
  image = docker_image.syncthing.image_id

  restart = "always"

  env = ["PUID=1000", "PGID=1000"]

  volumes {
    container_path = "/var/syncthing"
    host_path = "/docker_volumes/syncthing"
  }

  ports { # Web UI
    internal = 8384
    external = 8384
    protocol = "tcp"
  }

  ports { # TCP file transfer
    internal = 22000
    external = 22000
    protocol = "tcp"
  }

  ports { # UDP file transfer
    internal = 22000
    external = 22000
    protocol = "udp"
  }

  ports { # Local discovery broadcast
    internal = 21027
    external = 21027
    protocol = "udp"
  }
}

resource "docker_container" "minio" {
  name  = "minio"
  image = docker_image.minio.image_id

  restart = "always"

  command = [ "server", "/data", "--console-address", ":9090" ]

  env = ["MINIO_ROOT_USER=test-s3", "MINIO_ROOT_PASSWORD=test-s3"]

  volumes {
    container_path = "/data"
    host_path = "/docker_volumes/s3"
  }

  ports { # Web UI
    internal = 9090
    external = 9090
    protocol = "tcp"
  }

  ports { # S3
    internal = 9000
    external = 9000
    protocol = "tcp"
  }

}