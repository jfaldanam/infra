variable "ip" {
    type = string
    description = "IP address of the host."
}

variable "port" {
    type = number
    description = "Port of the ssh daemon on the host."
    default = 22
}

variable "user" {
    type = string
    description = "User to connect to the host through ssh."
}

variable "ssh_key" {
    type = string
    description = "Path of the ssh private key to connect to the host."
    sensitive = true
}

