resource "docker_container" "portainer" {
  name  = "portainer"
  image = docker_image.portainer.image_id

  restart = "always"

  volumes {
    container_path = "/var/run/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    container_path = "/data"
    host_path = "/docker_volumes/portainer"
  }

  ports {  # Web UI
    internal = 9443
    external = 9443
    protocol = "tcp"
  }
}

resource "docker_container" "gitlab-runner" {
  name  = "gitlab-runner"
  image = docker_image.gitlab-runner.image_id

  restart = "always"

  volumes {
    container_path = "/var/run/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    container_path = "/etc/gitlab-runner"
    host_path = "/docker_volumes/gitlab-runner"
  }
}

resource "docker_container" "minio" {
  name  = "minio"
  image = docker_image.minio.image_id

  restart = "always"

  command = [ "server", "/data", "--console-address", ":9090" ]

  env = ["MINIO_ROOT_USER=test-s3", "MINIO_ROOT_PASSWORD=test-s3-pass"]

  volumes {
    container_path = "/data"
    host_path = "/docker_volumes/s3"
  }

  ports { # Web UI
    internal = 9090
    external = 9090
    protocol = "tcp"
  }

  ports { # S3
    internal = 9000
    external = 9000
    protocol = "tcp"
  }

}