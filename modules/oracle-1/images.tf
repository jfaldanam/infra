resource "docker_image" "portainer" {
  name = "portainer/portainer-ce:2.16.2"
}

resource "docker_image" "gitlab-runner" {
  name = "gitlab/gitlab-runner:v15.5.2"
}

resource "docker_image" "minio" {
  name = "minio/minio:RELEASE.2023-07-21T21-12-44Z"
}